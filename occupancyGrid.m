
%This file uses occupancy grid mapping to map out the environment of a 30cm
%by 30cm robot with 6 infra red sensors mounted, instead of a laser
%scanner.
%Author: Jawad Ateeq

degToRad = 2* pi / 180;
numSensors = 6;

%sensors attachement on robot relative to center of robot
sensors = [  1.5    1  -pi/2 
             1.5    2  -pi/4 
             0.5    2   0 
            -0.5    2   0 
            -1.5    2   pi/4              
            -1.5    1   pi/2 ]; 
        
rmax = 15;                 % Max range of IR sensors
alpha = 1;                 % Measurement beam range uncertainty
beta =  7 * degToRad;      % Measurement angle uncertainty

% Simulation time
Tmax = 150;
T = 0:Tmax;

% Initial Robot location
x0 = [5 20 0];

%State Initialization
x = zeros(3,length(T)+1);
x(:,1) = x0;

% Robot motions
u = [3 0 -3 0;
     0 3 0 -3];
ui=1;

% Robot sensor rotation command
w = 0.3*ones(length(T));

%run ( './map.m' );
M = 30;
N = 30;

%initialize probability map
m = 0.5 * ones(M,N);
L0 = log(m./(1-m));
L = L0;

robot_body = zeros(3,3);        %robot structure matters
%pose = [ 15 25 3*pi/2 ];      %robot center pose 
pose = [ 15 25 3*pi/2 ];      %robot center pose 
meas_r = [ 10 15 15 15 8 12];   %measurements made by each sensor in the given robot pose

%get the inverse measurement model probability map
%Q: how do you deal with multiple sensor data overlap?
[ invMap ] = IMM( M , N , meas_r, sensors , pose , rmax , alpha , beta );

m = L + log(invMap./(1-invMap)) - L0;

% Map and vehicle path
figure(2);clf; hold on;
image(100*(1-m)');
colormap(gray);
axis([0 M 0 N])
title('Occupancy grid mapping');

% Map and vehicle path
figure(3);clf; hold on;
image(100*(1-invMap)');
colormap(gray);
axis([0 M 0 N]);
title('Inverse Measurement Model Probabilities');
