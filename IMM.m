% Inverse measurement model

% @PARAMS:

% meas: the measurement made by thy sensor. Should this be the 
% aggregate of all the measurements, or will these measurements be provided
% one at a time? If it is the former, then there needs to be some way of
% communicating the pose of the robot at each meas point.

% pose: the pose of the robot at which the meas was made

% rmax: max range of sensor. Adv of passing this in is that this function 
% can be used for multiple types of sensors (as long as they are all IR)

% alpha: The uncretainty in range in meters

% beta: The uncertainty in beam angle in degrees

function [ invMap ] = IMM( M, N, meas_r, sensors , pose , rmax , alpha, beta )
   
    invMap = zeros( M , N ); %inverse measurement model probabilities
    x = pose(1);
    y = pose(2);
    q = pose(3);

    for i = 1:M
        for j = 1:N
            %figure out distance and heading of current cell from
            %robot center
            %TODO: rm should be distance from sensor selected, not the
            %robot center
            rc = sqrt ( (x - i)^2 + ( y - j)^2 );
            qc = mod( atan2( j - y , i - x ) - q + pi , 2*pi ) - pi;

            %doing this instead of the way prof did since for cells very
            %close to the sensor, the angle to center if often larger than
            %relative angle to sensor and this gives incorrect results
            qToS = mod( atan2( j - y - sensors(1:end,2) , i - x - sensors(1:end,1) ) - q - sensors(1:end,3) + pi , 2*pi ) - pi;
            rToS = sqrt ( (x + sensors(1:end,1) - i).^2 + ( y + sensors(1:end,2) - j).^2 );        
            
            %find index of closest measurement heading
            [ meas_cur , k ] = min( abs( qToS ) );

            %if either of the sensors pointing at 0 were chosen, 
            %there's a possiblity the right one may not have been found.
            %select proper one based on proximity of map cell to beam
            %if ( k == 3 || k == 4 )
                
            %end
                        
            rm = rToS( k ); % Distance to chosen sensor
            qm = qToS( k ); % Heading to chosen sensor
            
            if ( ( rc - 1.5 ) <= 0 )
                %space occupied by robot.. assume a radius of 0.212 around
                %center that is occupied by robot body
                invMap( i , j ) = 0.2;
            elseif ( rm > min( rmax , meas_r( k ) + alpha/2 ) || ...                  
                     abs( qm ) > beta/2 )
                invMap( i , j ) = 0.5;
            elseif ( meas_r( k ) < rmax && abs( meas_r( k ) - rm ) <= alpha/2 )
                invMap( i , j ) = 0.7; %obstacle observed             
            elseif ( rm < meas_r(k) )
                %TODO: it would be better to assign higher prob to points
                %closer to the robot in the beam as opposed to ones farther
                %away
                invMap( i , j ) = 0.4; %free space
            end            
        end        
    end
end
            
            
    

